<%#                                                                          -%>
<%# File informations:                                                       -%>
<%# - Name:    elems/plugins.vim.tt                                          -%>
<%# - Summary: Include Vim plugins.                                          -%>
<%# - Authors:                                                               -%>
<%#   - Alessandro Molari <molari.alessandro@gmail.com> (alem0lars)          -%>
<%#   - Luca Molari <molari.luca@gmail.com> (LMolr)                          -%>
<%#                                                                          -%>
<%# Project informations:                                                    -%>
<%#   - Homepage:                                                            -%>
<%#       https://gitlab.com/fizzycfg/official-cfg/configs-vim               -%>
<%#   - Getting started: see README.md in the project root folder            -%>
<%#                                                                          -%>
<%# License: Apache v2.0                                                     -%>
<%#                                                                          -%>
<%# Licensed to the Apache Software Foundation (ASF) under one more          -%>
<%# contributor license agreements.  See the NOTICE file distributed with    -%>
<%# this work for additional information regarding copyright ownership.      -%>
<%# The ASF licenses this file to you under the Apache License, Version 2.0  -%>
<%# (the "License"); you may not use this file except in compliance with the -%>
<%# License.                                                                 -%>
<%# You may obtain a copy of the License at                                  -%>
<%#     http://www.apache.org/licenses/LICENSE-2.0                           -%>
<%# Unless required by applicable law or agreed to in writing, software      -%>
<%# distributed under the License is distributed on an "AS IS" BASIS,        -%>
<%# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied  -%>
<%# See the License for the specific language governing permissions and      -%>
<%# limitations under the License.                                           -%>
<%#                                                                          -%>
<%
  define_locals do
    variable "vim.vimplug.cache_dir", type: :directory, as: :cache_dir
  end
-%>

call plug#begin('<%= local! :cache_dir %>')

" Editing plugins.
call plugins#context_filetype#declare()
call plugins#repeat#declare()
call plugins#surround#declare()
call plugins#unimpaired#declare()
call plugins#abolish#declare()
call plugins#speeddating#declare()
call plugins#tcomment#declare()
call plugins#lexima#declare()
call plugins#easyalign#declare()
call plugins#multiple_cursors#declare()
call plugins#easymotion#declare()
call plugins#incsearch#declare()
call plugins#over#declare()
call plugins#bbye#declare()
call plugins#nrrwrgn#declare()
call plugins#table_mode#declare()
call plugins#better_whitespace#declare()
call plugins#fastfold#declare()
call plugins#foldtext#declare()
call plugins#drawit#declare()
<% if has_feature? :figlet %>
call plugins#figlet#declare()
<% end %>
call plugins#autotype#declare()
call plugins#cleverf#declare()
call plugins#txtfmt#declare()
call plugins#ansiesc#declare()
call plugins#targets#declare()

" UI plugins.
call plugins#tagbar#declare()
call plugins#gitgutter#declare() " must be before signature
call plugins#signature#declare()
call plugins#choosewin#declare()
call plugins#windowswap#declare()
call plugins#taboo#declare()
"call plugins#numbers#declare() " TODO slows down vim
call plugins#denite#declare()

" Syntax Check plugins.
call plugins#ale#declare()

" Template plugins.
<% if has_feature? :vimtemplates -%>
call plugins#template#declare()
<% end -%>

" AutoComplete & Snippets plugins.
call plugins#deoplete#declare()
call plugins#neosnippet#declare()

" Doc plugins.
call plugins#man#declare()
<% if has_feature? :dasht %>
call plugins#dasht#declare()
<% end %>

" Note-taking and task plugins.
call plugins#vimoutliner#declare()
<% if has_feature? :vimwiki %>
call plugins#vimwiki#declare()
<% end %>
<% if has_feature? :taskwarrior %>
call plugins#taskwarrior#declare()
<% end %>

" Programming language plugins.
call plugins#langs#vimscript#declare()
call plugins#langs#toml#declare()
call plugins#langs#mediawiki#declare()
call plugins#langs#graphql#declare()
<% if has_feature? :c, :cpp, :objc, :objcpp, match: :any? %>
call plugins#langs#c_based#declare()
<% end %>
<% if has_feature? :ruby %>
call plugins#langs#ruby#declare()
<% end %>
<% if has_feature? :haskell %>
call plugins#langs#haskell#declare()
<% end %>
<% if has_feature? :rust %>
call plugins#langs#rust#declare()
<% end %>
<% if has_feature? :python %>
call plugins#langs#python#declare()
<% end %>
<% if has_feature? :javascript %>
call plugins#langs#javascript#declare()
<% end %>
<% if has_feature? :typescript %>
call plugins#langs#typescript#declare()
<% end %>
<% if has_feature? :coffeescript %>
call plugins#langs#coffeescript#declare()
<% end %>
<% if has_feature? :powershell %>
call plugins#langs#powershell#declare()
<% end %>
<% if has_feature? :tex %>
call plugins#langs#tex#declare()
<% end %>
<% if has_feature? :tmux %>
call plugins#langs#tmux#declare()
<% end %>
<% if has_feature? :plantuml %>
call plugins#langs#plantuml#declare()
<% end %>
<% if has_feature? :systemd %>
call plugins#langs#systemd#declare()
<% end %>
<% if has_feature? :bro %>
call plugins#langs#bro#declare()
<% end %>
<% if has_feature? :salt %>
call plugins#langs#salt#declare()
<% end %>
<% if has_feature? :xml %>
call plugins#langs#xml#declare()
<% end %>
<% if has_feature? :markdown %>
call plugins#langs#markdown#declare()
<% end %>
<% if has_feature? :elixir -%>
call plugins#langs#elixir#declare()
<% end -%>

" Shell plugins.
<% if has_feature? :neovim %>
call plugins#iron#declare()
<% end %>

" VCS plugins.
call plugins#fugitive#declare()

" Undo plugins.
call plugins#undotree#declare()
call plugins#peekaboo#declare()

" Yank plugins.
call plugins#highlightedyank#declare()

" File management plugins.
call plugins#sudoedit#declare()
call plugins#rooter#declare()
call plugins#fzf#declare()
call plugins#far#declare()
call plugins#grepper#declare()
call plugins#eunuch#declare()

" Misc plugins.
call plugins#prettyprint#declare()

" Status line plugins.
call plugins#airline#declare()

" Colorscheme plugins.
call plugins#colorscheme#declare()

call plug#end()

" Editing plugins.
call plugins#surround#configure()
call plugins#tcomment#configure()
call plugins#easyalign#configure()
call plugins#multiple_cursors#configure()
call plugins#easymotion#configure()
call plugins#incsearch#configure()
call plugins#over#configure()
call plugins#bbye#configure()
call plugins#better_whitespace#configure()
call plugins#fastfold#configure()
call plugins#foldtext#configure()
call plugins#drawit#configure()
call plugins#cleverf#configure()
call plugins#txtfmt#configure()
call plugins#ansiesc#configure()
call plugins#targets#configure()

" UI plugins.
call plugins#tagbar#configure()
call plugins#gitgutter#configure()
call plugins#signature#configure()
call plugins#choosewin#configure()
call plugins#windowswap#configure()
call plugins#taboo#configure()
"call plugins#numbers#configure()
call plugins#denite#configure()

" Syntax Check plugins.
call plugins#ale#configure()

" Template plugins.
<% if has_feature? :vimtemplates -%>
call plugins#template#configure()
<% end -%>

" AutoComplete & Snippets plugins.
call plugins#deoplete#configure()
call plugins#neosnippet#configure()

" Doc plugins.
call plugins#man#configure()
<% if has_feature? :dasht %>
call plugins#dasht#configure()
<% end %>

" Note-taking and task plugins.
call plugins#vimoutliner#configure()
<% if has_feature? :vimwiki %>
call plugins#vimwiki#configure()
<% end %>
<% if has_feature? :taskwarrior %>
call plugins#taskwarrior#configure()
<% end %>

" Programming language plugins.
call plugins#langs#vimscript#configure()
call plugins#langs#graphql#configure()
<% if has_feature? :c, :cpp, :objc, :objcpp, match: :any? %>
call plugins#langs#c_based#configure()
<% end %>
<% if has_feature? :ruby %>
call plugins#langs#ruby#configure()
<% end %>
<% if has_feature? :python %>
call plugins#langs#python#configure()
<% end %>
<% if has_feature? :haskell %>
call plugins#langs#haskell#configure()
<% end %>
<% if has_feature? :rust %>
call plugins#langs#rust#configure()
<% end %>
<% if has_feature? :javascript %>
call plugins#langs#javascript#configure()
<% end %>
<% if has_feature? :typescript %>
call plugins#langs#typescript#configure()
<% end %>
<% if has_feature? :coffeescript %>
call plugins#langs#coffeescript#configure()
<% end %>
<% if has_feature? :powershell %>
call plugins#langs#powershell#configure()
<% end %>
<% if has_feature? :tex %>
call plugins#langs#tex#configure()
<% end %>
<% if has_feature? :plantuml %>
call plugins#langs#plantuml#configure()
<% end %>
<% if has_feature? :bro %>
call plugins#langs#bro#configure()
<% end %>
<% if has_feature? :salt %>
call plugins#langs#salt#configure()
<% end %>
<% if has_feature? :xml %>
call plugins#langs#xml#configure()
<% end %>
<% if has_feature? :markdown %>
call plugins#langs#markdown#configure()
<% end %>
<% if has_feature? :elixir -%>
call plugins#langs#elixir#configure()
<% end -%>

" Shell plugins.
<% if has_feature? :neovim %>
call plugins#iron#configure()
<% end %>

" Undo plugins.
call plugins#undotree#configure()
call plugins#peekaboo#configure()

" Yank plugins.
call plugins#highlightedyank#configure()

" File management plugins.
call plugins#fzf#configure()
call plugins#far#configure()
call plugins#grepper#configure()
call plugins#eunuch#configure()

" Status line plugins.
call plugins#airline#configure()

call plugins#colorscheme#configure()
