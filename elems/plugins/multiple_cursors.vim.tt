<%#                                                                          -%>
<%# File informations:                                                       -%>
<%# - Name:    elems/plugins/multiple_cursors.vim.tt                         -%>
<%# - Summary: Configure plugins `terryma/vim-multiple-cursors`.             -%>
<%# - Authors:                                                               -%>
<%#   - Alessandro Molari <molari.alessandro@gmail.com> (alem0lars)          -%>
<%#   - Luca Molari <molari.luca@gmail.com> (LMolr)                          -%>
<%#                                                                          -%>
<%# Project informations:                                                    -%>
<%#   - Homepage:                                                            -%>
<%#       https://gitlab.com/fizzycfg/official-cfg/configs-vim               -%>
<%#   - Getting started: see README.md in the project root folder            -%>
<%#                                                                          -%>
<%# License: Apache v2.0                                                     -%>
<%#                                                                          -%>
<%# Licensed to the Apache Software Foundation (ASF) under one more          -%>
<%# contributor license agreements.  See the NOTICE file distributed with    -%>
<%# this work for additional information regarding copyright ownership.      -%>
<%# The ASF licenses this file to you under the Apache License, Version 2.0  -%>
<%# (the "License"); you may not use this file except in compliance with the -%>
<%# License.                                                                 -%>
<%# You may obtain a copy of the License at                                  -%>
<%#     http://www.apache.org/licenses/LICENSE-2.0                           -%>
<%# Unless required by applicable law or agreed to in writing, software      -%>
<%# distributed under the License is distributed on an "AS IS" BASIS,        -%>
<%# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied  -%>
<%# See the License for the specific language governing permissions and      -%>
<%# limitations under the License.                                           -%>
<%#                                                                          -%>
<%
  define_locals do
  end
-%>

function! plugins#multiple_cursors#declare() abort
  Plug 'terryma/vim-multiple-cursors'
endfunction

function! plugins#multiple_cursors#configure() abort
  " Highlight the virtual cursors and their visual selections.
  highlight multiple_cursors_cursor term=reverse cterm=reverse gui=reverse
  highlight link multiple_cursors_visual Visual
endfunction
